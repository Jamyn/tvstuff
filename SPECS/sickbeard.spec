%global python26_sitelib %{_libdir}/python2.6/site-packages

Summary: Sick Beard
Name: sickbeard
Version: 503
Release: 6%{?dist}
License: Python license
Group: Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Prefix: %{_prefix}
BuildArch: noarch
Url: https://github.com/midgetspy/Sick-Beard
BuildRequires: python-devel
Requires(pre): /usr/sbin/useradd, /usr/bin/getent
Requires(post): initscripts
Requires(post): /sbin/chkconfig
Requires(postun): /usr/sbin/userdel
Requires: python >= 2.6
Requires: python-cheetah
Source0: https://github.com/midgetspy/Sick-Beard/archive/build-%{version}.tar.gz
Source1: sickbeard.sysconfig
AutoReq: no

%description
The ultimate PVR application that searches for and manages your TV shows

%prep
%setup -q -n Sick-Beard-build-%{version}

%build

%install
rm -rf $RPM_BUILD_ROOT/*
mkdir -p $RPM_BUILD_ROOT/opt/sickbeard $RPM_BUILD_ROOT/etc/init.d
install -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT/etc/sysconfig/sickbeard
install -D -m 755 init.fedora $RPM_BUILD_ROOT/etc/init.d/sickbeard
mv * $RPM_BUILD_ROOT/opt/sickbeard

%pre
/usr/bin/getent group sickbeard >/dev/null || /usr/sbin/groupadd -r sickbeard
/usr/bin/getent passwd sickbeard >/dev/null || /usr/sbin/useradd -g sickbeard -r -M -d /opt/sickbeard -s /sbin/nologin sickbeard

%post
/sbin/chkconfig --add sickbeard

%preun 
/sbin/service sickbeard stop &>/dev/null || :
/sbin/chkconfig --del sickbeard

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,sickbeard,sickbeard)
/opt/sickbeard/*
%defattr(-,root,root)
/etc/init.d/sickbeard
%config(noreplace) /etc/sysconfig/sickbeard

%changelog
* Tue Aug 05 2014 Jamyn Shanley <jamyn@shanley.org> 503-6
- Shamelessly based off of https://github.com/funzoneq/rpmspecs/
- Ideas stolen from: http://deviantengineer.com/2014/02/sickbeard-from-source/
- Updated to new version
- Use official Sick Beard versions (vs master)
- Clean up user/group creation
- /opt/sickbeard should be owned by sickbeard
- Add chkconfig support
- Don't auto-start sickbeard
- Fix typo in %preun

* Wed Aug 07 2013 Arnoud Vermeer <a.vermeer@freshway.biz> 498-3
- RPM, don't be smart. Don't auto add requirements (a.vermeer@freshway.biz)

* Wed Aug 07 2013 Arnoud Vermeer <a.vermeer@freshway.biz> 498-2
- Adding source (a.vermeer@freshway.biz)

* Mon Aug 05 2013 Arnoud Vermeer <a.vermeer@freshway.biz> 498-1
- new package built with tito
