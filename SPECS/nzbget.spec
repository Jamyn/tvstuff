Name:		nzbget
Version:	13.0
Release:	1%{?dist}
Summary:	Command-line based binary newsgrabber for nzb files

Group:		Applications/Internet
License:	GPLv2+
URL:		http://nzbget.sourceforge.net/
Source0:	http://downloads.sourceforge.net/project/nzbget/nzbget-stable/%{version}/nzbget-%{version}.tar.gz
Source1:	nzbget.sysconfig
BuildRequires:	gcc gcc-c++
BuildRequires:	libxml2-devel par2cmdline
BuildRequires:	ncurses-devel gnutls-devel
BuildRequires:	libsigc++20-devel
BuildRequires:	ncurses-devel

%description
NZBGet is a command-line based binary newsgrabber for nzb files, written in C++.
It supports client/server mode, automatic par-check/-repair.
NZBGet requires low system resources and runs great on routers and NAS-devices.

NZBGet can be used in standalone and in server/client modes.
In standalone mode NZBGet downloads listed files and then exits.
In server/client mode NZBGet runs as server in background. Clients send requests
such as downloading an NZB file, listing files in queue, etc.

Standalone-tool, server and client are all contained in a single binary.

%prep
%setup -q

%pre
/usr/bin/getent group nzbget >/dev/null || /usr/sbin/groupadd -r nzbget
/usr/bin/getent passwd nzbget >/dev/null || /usr/sbin/useradd -g nzbget -r -M -d /opt/nzbget -s /sbin/nologin nzbget

%post
/sbin/chkconfig --add nzbget

%build
# No exception for linking against OpenSSL in nzbget's license.
%configure --with-tlslib=GnuTLS
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
install -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT/etc/sysconfig/nzbget

%files
%defattr(-,root,root,-)
%doc %{_datadir}/doc/nzbget/*
%{_bindir}/nzbget
%{_sbindir}/*
%{_datadir}/nzbget/scripts/*
%{_datadir}/nzbget/webui/*
%{_datadir}/nzbget/nzbget.conf

%preun
/sbin/service nzbget stop &>/dev/null || :
/sbin/chkconfig --del nzbget

%changelog
* Wed Aug 05 2014 Jamyn Shanley <jamyn@shanley.org> 13.0-1
- CentOS 6 build of nzbget 13.0

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Thu Oct 28 2010 Pierre Carrier <prc@redhat.com> 0.7.0-1
- Initial packaging
