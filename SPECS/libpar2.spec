Name: libpar2
Version: 0.4       
Release: 1%{?dist}
Summary: Library for performing comman tasks related to PAR recovery sets
     
Group: System Environment/Libraries
License: GPLv2+        
URL: https://launchpad.net/libpar2 
Source0: https://launchpad.net/libpar2/trunk/%{version}/+download/libpar2-%{version}.tar.gz         
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
  
BuildRequires: libsigc++20-devel
BuildRequires: sed

%description
LibPar2 allows for the generation, modification, verification,
and repair of PAR v1.0 and PAR v2.0(PAR2) recovery sets.
It contains the basic functions needed for working with these
sets and is the basis for GUI applications such as GPar2.


%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libsigc++20-devel

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q
#fix source files
chmod -x *.cpp *.h ChangeLog
touch tmpfile -r README 
sed -i 's/\r//' README
touch -r tmpfile README
touch tmpfile -r ROADMAP 
sed -i 's/\r//' ROADMAP
touch -r tmpfile ROADMAP
touch tmpfile -r AUTHORS
sed -i 's/\r//' AUTHORS
touch -r tmpfile AUTHORS

%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'


%clean
rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%{_libdir}/*.so.*
%doc COPYING README ChangeLog AUTHORS ROADMAP

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/include/

%changelog
* Wed Aug 05 2014 Jamyn Shanley <jamyn@shanley.org> - 0.4-1
- Rebuilt against newer version: https://launchpad.net/libpar2

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Feb 28 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-12
- Rebuilt for c++ ABI breakage

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Aug 29 2008 Michael Schwendt <mschwendt@fedoraproject.org> 0.2-7
- Include %%_libdir/libpar2 directory

* Sat Jun 21 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.2-6
- Add missing requires to devel package (RH #452363)

* Sat Feb 09 2008 Adel Gadllah <adel.gadllah@gmail.com> 0.2-5
- Rebuild for gcc-4.3

* Sat Dec 29 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.2-4
- Preserve doc timestamps

* Sat Dec 29 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.2-3
- Drop makefile patch
- Some cleanups

* Sat Dec 29 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.2-2
- Preserve timestamps
- Package docs
- Fix description

* Fri Dec 28 2007 Adel Gadllah <adel.gadllah@gmail.com> 0.2-1
- Initial package
